import { Injectable } from '@angular/core';
import { Feedback } from '../shared/feedback';
//import { PROMOTIONS } from '../shared/promotions';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { RestangularModule, Restangular } from 'ngx-restangular';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/delay';

@Injectable()
export class FeedbackService {

  constructor( private restangular: Restangular,
      private processHTTPMsgService: ProcessHTTPMsgService){} 

  submitFeedback(Feedback) {
    return this.restangular.all('Feedback').post();
  }
}