import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { LEADERS } from '../shared/leaders';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { RestangularModule, Restangular } from 'ngx-restangular';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/delay';

@Injectable()
export class LeaderService {

  constructor( private restangular: Restangular,
      private processHTTPMsgService: ProcessHTTPMsgService){} 

  getLeaders(): Observable<Leader[]> {
    return this.restangular.all('LEADERS').getList();
  }

  getLeader(id: number): Observable<Leader> {
    rreturn  this.restangular.one('LEADERS',id).get();
  }

  getFeaturedLeader(): Observable<Leader> {
    return this.restangular.all('LEADERS').getList({featured: true})
      .map(LEADERS => LEADERS[0]);
  }
}