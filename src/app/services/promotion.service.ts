import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';
import { PROMOTIONS } from '../shared/promotions';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { RestangularModule, Restangular } from 'ngx-restangular';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/delay';

@Injectable()
export class PromotionService {

  constructor( private restangular: Restangular,
      private processHTTPMsgService: ProcessHTTPMsgService){} 

  getPromotions(): Observable<Promotion[]> {
    return this.restangular.all('PROMOTIONS').getList();
  }

  getPromotion(id: number): Observable<Promotion> {
    return  this.restangular.one('PROMOTIONS',id).get();
  }

  getFeaturedPromotion(): Observable<Promotion> {
    return this.restangular.all('PROMOTIONS').getList({featured: true})
      .map(PROMOTIONS => PROMOTIONS[0]);
  }
}